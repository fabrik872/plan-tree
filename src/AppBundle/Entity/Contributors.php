<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contributors
 *
 * @ORM\Table(name="contributors")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContributorsRepository")
 */
class Contributors
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="contributors")
     * @ORM\JoinColumn(name="projectId", referencedColumnName="id")
     */
    private $projectId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var bool
     *
     * @ORM\Column(name="isAdmin", type="boolean")
     */
    private $isAdmin;

    /**
     * @var bool
     *
     * @ORM\Column(name="isWritable", type="boolean")
     */
    private $isWritable;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param Project $projectId
     *
     * @return Contributors
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return Project
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set userId
     *
     * @param User $userId
     *
     * @return Contributors
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return Contributors
     */
    public function setisAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return bool
     */
    public function getisAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set isWritable
     *
     * @param boolean $isWritable
     *
     * @return Contributors
     */
    public function setIsWritable($isWritable)
    {
        $this->isWritable = $isWritable;

        return $this;
    }

    /**
     * Get isWritable
     *
     * @return bool
     */
    public function getIsWritable()
    {
        return $this->isWritable;
    }
}

