<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $projects = $this->getDoctrine()->getRepository(Project::class)->newestProjects();
        return $this->render('default/welcome.html.twig', [
            "projects" => $projects
        ]);
    }

    /**
     * @Route("/my-projects", name="myProjects")
     */
    public function publicProjectsAction(Request $request)
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        return $this->render('default/index.html.twig', [
            "projects" => $user->getProject(),
        ]);
    }
}
