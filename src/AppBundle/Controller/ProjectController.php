<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contributors;
use AppBundle\Entity\Nodes;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Project controller.
 *
 * @Route("project")
 */
class ProjectController extends Controller
{
    /**
     * Creates a new project entity.
     *
     * @Route("/create", name="project_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm('AppBundle\Form\ProjectType', $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $project->setLastUpdateDate(new \DateTime());
            $project->setCreatorId($this->getUser());
            $em->persist($project);
            $em->flush();
            $this->addFlash('success', 'Project: ' . $project->getName() . ' created');

            return $this->redirectToRoute('project_show', array('id' => $project->getId()));
        }

        return $this->render('project/new.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/{id}", name="project_show")
     * @Method("GET")
     */
    public function showAction(Project $project)
    {
        return $this->render('project/show.html.twig', array(
            'project' => $project,
        ));
    }

    /**
     * @Route("/{id}/show", name="project_open")
     * @Security("has_role('ROLE_USER')")
     * @Method("POST")
     */
    public function openAction(Project $project, Request $request)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $project, true);

        /*add node form processing*/
        $addNodeForm = $this->createAddNodeForm($request, $project);
        if (!is_array($addNodeForm)) {
            return $addNodeForm;
        }

        /*add contributors form processing*/
        $addContributorForm = $this->createAddContributor($request, $project);
        if (!is_array($addContributorForm)) {
            return $addContributorForm;
        }

        /*edit node form processing*/
        $editNodeForms = [];
        foreach ($project->getNodes() as $node) {
            /**
             * @var $node Nodes
             */
            $editNodeForm = $this->createEditNodeForm($request, $node, $project);
            if (!is_array($editNodeForm)) {
                return $editNodeForm;
            }
            $editNodeForms[$node->getId()] = $editNodeForm;
        }

        /*generating tree*/
        $parents = $this->getDoctrine()->getRepository(Nodes::class)->getChildren($project);
        $tree = $this->generateTree($parents, $project);

        return $this->render('project/open.html.twig', array(
            'dataTree' => $tree,
            'project' => $project,
            'node' => $addNodeForm['node'],
            'addNodeForm' => $addNodeForm['form']->createView(),
            'editNodeForms' => $editNodeForms,
        ));
    }

    /**
     * Displays a form to edit an existing project entity.
     *
     * @Route("/{id}/edit", name="project_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, Project $project)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $project);

        $deleteForm = $this->createDeleteForm($project);
        $editForm = $this->createForm('AppBundle\Form\ProjectType', $project);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Project: ' . $project->getName() . ' updated');

            return $this->redirectToRoute('project_edit', array('id' => $project->getId()));
        }

        return $this->render('project/edit.html.twig', array(
            'project' => $project,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'contributors' => $project->getContributors(),
        ));
    }

    /**
     * Deletes a project entity.
     *
     * @Route("/delete/{id}", name="project_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction(Request $request, Project $project)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $project);

        $form = $this->createDeleteForm($project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($project);
            $em->flush();
            $this->addFlash('success', 'Project: ' . $project->getName() . ' deleted');
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Deletes a node entity.
     *
     * @Route("/delete/node/{id}", name="project_nodes_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteNodeAction(Request $request, Nodes $node)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $node->getProjectId());

        $form = $this->createDeleteNodeForm($node);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($node);
            $em->flush();
            $this->addFlash('success', 'Node: ' . $node->getName() . ' deleted');
        }

        return $this->redirectToRoute('project_open', array('id' => $node->getProjectId()->getId()));
    }

    /**
     * Creates a form to delete a project entity.
     *
     * @param Project $project The project entity
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    private function createDeleteForm(Project $project)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('project_delete', array('id' => $project->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function createDeleteNodeForm(Nodes $node)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('project_nodes_delete', array('id' => $node->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function createAddNodeForm(Request $request, Project $project)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $project, true);

        $nodes = $project->getNodes();
        $nodeOptions["No parent"] = 0;
        foreach ($nodes as $value) {
            /**
             * @var $value Nodes
             */
            $nodeOptions[$value->getName()] = $value->getId();
        }

        $node = new Nodes();
        $form = $this->createForm('AppBundle\Form\NodesType', $node, ["attr" => $nodeOptions]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*authenticate*/
            $this->authenticate($this->getUser(), $project);

            $em = $this->getDoctrine()->getManager();

            $node->setProjectId($project);
            $node->setIsDone(false);
            $node->setworkerId($this->getUser());

            $em->persist($node);
            $em->flush();
            $this->addFlash('success', 'Node: ' . $node->getName() . ' created');

            return $this->redirectToRoute('project_open', array('id' => $project->getId()));
        }
        return [
            'node' => $node,
            'form' => $form,
        ];
    }

    private function createAddContributor(Request $request, Project $project)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $project, true);

        $contributor = new Contributors();

        if ($request->get('addContributor')) {
            $searchedUser = $this->getDoctrine()->getRepository(User::class)->find(intval($request->get('addContributor')));
            if ($searchedUser) {
                /*authenticate*/
                $this->authenticate($this->getUser(), $project);

                $em = $this->getDoctrine()->getManager();

                $contributor->setUserId($searchedUser);
                $contributor->setProjectId($project);
                $contributor->setisAdmin(false);
                $contributor->setIsWritable(false);

                $em->persist($contributor);
                $em->flush();
                $this->addFlash('success', 'Contributor: ' . $searchedUser->getName() . ' created');

                return $this->redirectToRoute('project_open', array('id' => $project->getId()));
            } else {
                $this->addFlash("danger", "Cannot find user");
            }
        }
        return [
            'contributor' => $contributor,
        ];
    }

    private function createEditNodeForm(Request $request, Nodes $node, Project $project)
    {
        /*authenticate*/
        $this->authenticate($this->getUser(), $project, true);

        $nodes = $project->getNodes();
        $nodeOptions["No parent"] = 0;
        foreach ($nodes as $value) {
            /**
             * @var $value Nodes
             */
            if ($node->getId() != $value->getId()) {
                $nodeOptions[$value->getName()] = $value->getId();
            }
        }

        $deleteForm = $this->createDeleteNodeForm($node);
        $editForm = $this->createForm('AppBundle\Form\NodesEditType', $node, ["attr" => $nodeOptions]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            /*authenticate*/
            $this->authenticate($this->getUser(), $project);

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Node: ' . $node->getName() . ' updated');

            return $this->redirectToRoute('project_open', array('id' => $project->getId()));
        }

        return [
            'node' => $node,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    private function generateTree(array $parents, Project $project)
    {
        $tree = [];
        foreach ($parents as $parent) {
            /**
             * @var $parent Nodes
             * @var $child array|Nodes
             */

            $child = $this->getDoctrine()->getRepository(Nodes::class)->getChildren($project, $parent->getId());
            $tree[$parent->getId()] = [$parent];

            if ($child != []) {
                $tree[$parent->getId()][] = $this->generateTree($child, $project);
            }
        }

        return $tree;
    }

    private function authenticate(User $thisUser, Project $project, bool $read = false)
    {
        $denied = true;
        foreach ($project->getContributors() as $contributor) {
            /**
             * @var $contributor Contributors
             */
            if ($thisUser->getId() == $contributor->getUserId()->getId()) {
                if ($contributor->getisAdmin()) {
                    $denied = false;
                } elseif ($contributor->getIsWritable() and $read) {
                    $denied = false;
                }
            }
        }

        if ($project->getIsPublic() and $read) {
            $denied = false;
        }
        if ($project->getCreatorId()->getId() == $thisUser->getId()) {
            $denied = false;
        }
        if ($denied) {
            throw $this->createAccessDeniedException('Access Denied');
        }
    }
}
