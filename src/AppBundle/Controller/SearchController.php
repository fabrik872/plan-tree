<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    /**
     * @Route("/json/search", name="ajaxSearchContributors")
     */
    public function ajaxAction(Request $request)
    {
        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {
            $project = $this->getDoctrine()->getRepository(Project::class)->find($request->get('projectId'));
            $results = $this->getDoctrine()->getRepository(User::class)->searchContributors($request->get('q'), $project);

            $jsonData = [];
            foreach ($results as $item) {
                /**
                 * @var $item User
                 */

                $jsonData[] = [
                    'path' => $this->generateUrl('project_open', ['id' => $project->getId()]),
                    'id' => $item[0]->getId(),
                    'name' => $item[0]->getName(),
                    'email' => $item[0]->getEmail(),
                    'score' => $item['score'],
                ];
            }

            return new JsonResponse($jsonData);
        } else {
            return $this->createNotFoundException();
        }
    }
}
