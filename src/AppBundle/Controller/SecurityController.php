<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {
        $helper = $this->get('security.authentication_utils');

        $errorMessage = $helper->getLastAuthenticationError();
        if ($errorMessage != '') {
            $this->addFlash('danger', $errorMessage->getMessageKey());
        }

        return $this->render(
            'auth/login.html.twig',
            array(
                'last_username' => $authenticationUtils->getLastUsername(),
            )
        );
    }

    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function loginCheckAction()
    {

    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {

    }
}